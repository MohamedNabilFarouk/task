<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'ProductsController@index')->name('all.products');
// Route::get('/', 'ProductsController@getData')->name('all.products');

Route::get('/getData', 'ProductsController@getData');

Route::get('createproduct','ProductsController@create')->name('product.create');
Route::get('editproduct/{product}','ProductsController@edit')->name('product.edit');
Route::get('showproduct/{product}','ProductsController@show')->name('product.show');

Route::post('addproduct','ProductsController@store')->name('product.store');
Route::put('updateproduct/{product}','ProductsController@update')->name('product.update');
Route::delete('deleteproduct/{product}','ProductsController@destroy')->name('product.delete');

