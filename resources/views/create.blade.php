@extends('mylayouts.master')


@section('title')
Create product
@endsection

@section('content')




<div class="grid grid-cols-12 gap-6 mt-5">
                <div class="intro-y col-span-12 lg:col-span-6">
                            <div class="intro-y box">
                            <div class="p-5" id="form-validation">
                            <div class="preview">
                                    <div class="">
                                    @if ($errors->any())

                                        <div class="alert alert-danger">

                                            <ul>

                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach

                                            </ul>

                                        </div>  

                                     @endif
                                                <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">

                                                                @csrf

                        

                                                                <div class="input-form">
                                                                                            <label class="flex flex-col sm:flex-row">Name</label>


                                                                                    <input type="text" name="name" class="form-control"  placeholder="product name" required>
       
                                                                </div>
                                                                <div class="input-form">
                                                                                            <label class="flex flex-col sm:flex-row">Description</label>
<textarea type="text" name="description" class="form-control"  placeholder="Description" required></textarea>
                                                                                    <!-- <input type="text" name="description" class="form-control"  placeholder="Description" required> -->
                                                                 </div>
                                                                 <div class="input-form">
                                                                                            <label class="flex flex-col sm:flex-row">Price</label>

                                                                                    <input type="text" name="price" class="form-control"  placeholder="Price" required>
                                                                 </div>
                                                                                     
                                                            </div>


                                                            <!-- main images -->

                                                            <div class="input-form">
                                                                    <label class="flex flex-col sm:flex-row" >Main image</label>
                                                                    <!-- <small> Upload image</small> -->

                                                            



                                                              


                                                                        <div style="width: 22%;background-color: white;padding:20px;margin: 5px;border-radius: 10px;">

                                                                            <label for="file-input">

                                                                            <i class="fa fa-plus imageInput " aria-hidden="true" style="background-color: #2681aa;border-radius: 50%;color:white;padding: 7px 10px;">upload</i>



                                                                            </label>

                                                                                    <input type="file" name="image" id="file-input"  style="display: none;" >

                                                                        </div>

                                                                    

                                                                    </div>

                                                                

                                                        

                                                            </div>



                                                            <!-- end main images -->


                                                        
                <!-- images -->

                <div class="input-form">
                                                                                    <label class="flex flex-col sm:flex-row" >Gallery Images</label>
                                                                                    <!-- <small> Upload image</small> -->

                                                                            



                                                                            


                                                                                        <div style="width: 22%;background-color: white;padding:20px;margin: 5px;border-radius: 10px;">

                                                                                         

                                                                                                    <input type="file" name="images[]"  multiple >
                                                                                                    

                                                                                        </div>

                                                                                    

                                                                                    </div>

                                                                                

                                                                        

                                                                            </div>



                                                                            <!-- end  images -->







                                                        







                                                            <div class="row row-wrap" style="display: flex;justify-content: center;">

                                                                

                                                                                        <div class="col-md-4  col-12 text-center " style="border-top: gray 1px solid; padding-top: 10px;width: 100%;">

                                                                                            <input type="submit" name="submit" value="submit" style="background-color: #025D8C;border: 0px;padding: 5px 10px;color: white;border-radius: 5px;">

                                                                                        </div>



                                                                                    </div>

                                                            

                                                            

                                                            </div>





                                                            

                                                            </form>

                                    </div>
                                </div>
                                </div>       
                            </div>         
                </div> 
</div>                   


@endsection
