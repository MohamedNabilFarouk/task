@extends('mylayouts.master')


@section('title')
Product
@endsection

@section('content')





<div class="intro-y overflow-auto lg:overflow-visible mt-8 sm:mt-0" >
<a href="{{route('product.create')}}" id="add" class="btn btn-success" style="margin:20px;">Add Product + </a>
                                <table id="example" class="table table-report sm:mt-2">
        <thead>
            
        <tr>
        <th>name</th>
            <th>image</th>
                <th>description</th>
                <th>price</th>

                <th>Action</th>
                
            </tr>
        </thead>
        <tbody >
           
        @foreach($products as $p)
            <tr class="intro-x">
            <td>{{$p->name}}</td>
           
                <td><img src="{{asset('storage/'. $p->image)}}" style="width:20%;border-radius: 50%;"></td>
                <td>{{$p->description}}</td>
                <td>{{$p->price}}</td>
                
                <td class="table-report__action w-56">
                <a class="flex items-center mr-3" href="{{route('product.edit',$p)}}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                <form action="{{route('product.delete',$p)}}" method="POST">
                                                    {{ method_field('DELETE') }}
                                                    @csrf
                                                    <a class="flex items-center text-theme-6" href="#" onclick="this.parentNode.submit()"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </a>
                                                    </form>
                                                    <a class="flex items-center text-theme-6" href="{{route('product.show',$p)}}" > <i data-feather="eye" class="w-4 h-4 mr-1"></i> Show </a>

                                                    
                  <!-- <a href="" class="btn btn-info">edit</a> -->
                  </td>
                 
            </tr>
         @endforeach
        </tbody>
       
</table>
</div>
@endsection