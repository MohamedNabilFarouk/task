@foreach($products as $p)
            <tr class="intro-x">
            <td>{{$p->name}}</td>
           
                <td><img src="{{asset('storage/'. $p->image)}}" style="width:20%;border-radius: 50%;"></td>
                <td>{{$p->description}}</td>
                <td>{{$p->price}}</td>
                
                <td class="table-report__action w-56">
                <a class="flex items-center mr-3" href="{{route('product.edit',$p)}}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                <form action="{{route('product.delete',$p)}}" method="POST">
                                                    {{ method_field('DELETE') }}
                                                    @csrf
                                                    <a class="flex items-center text-theme-6" href="#" onclick="this.parentNode.submit()"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </a>
                                                    </form>
                  <!-- <a href="" class="btn btn-info">edit</a> -->
                  </td>
                 
            </tr>
         @endforeach