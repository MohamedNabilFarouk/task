<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Product extends Model
{
    //
    use Notifiable;

    public function routeNotificationForMail($notification){
  $email = 'mohamednabilfarouk@gmail.com';
  // Return email address only
    return $email ;
  }

  public function images()
    {
        return $this->hasMany(Gallery::class);
    }

}
