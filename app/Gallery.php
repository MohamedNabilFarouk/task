<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //
    protected $table= 'product_images';
    public $timestamps =false;

    public function product()
    {
        return $this->hasOne(Product::class);
    }
}
