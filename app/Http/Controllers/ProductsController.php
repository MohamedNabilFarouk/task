<?php

namespace App\Http\Controllers;

use App\Product;
use App\Gallery;
use Illuminate\Http\Request;
use Session;
use Flash;
use DB;
use Notification;
use App\Notifications\ProductAdded;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $count = count(Product::all());
       $products =Product::all();
        return view('index', compact('count', 'products'));
    }

    public function getData()
    {
        //
        $products['data'] = Product::all();
        echo json_encode($products);
        exit;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'description' => 'required',
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'image' => 'required',
            'images'=> 'required'
            ]);
            if ($validator->fails()){
                $errors= $validator->errors();
                return view('create')->with('errors', $errors);
            }else{

        DB::transaction(function () use  ($request) {

        $products = new Product;
        $products->name = $request->input('name');
        $products->description = $request->input('description');
        $products->price = $request->input('price'); 
        if($request->hasFile('image')){
            $File= $request->file('image');
            $file_name = rand(1, 999) . $File -> getClientOriginalName() ;
            $file = $File->storeAs('Products',$file_name);
        $products->image=$file;
        }
        $products->save();

        if($request->hasFile('images')){
            $Files= $request->file('images');
            foreach($Files as $f){
                $file_name = rand(1, 999) . $f -> getClientOriginalName() ;
                $files = $f->storeAs('products',$file_name);
                $gallery= new Gallery;

                $gallery->image= $files;
                $gallery->product_id= $products->id;
                $gallery->save();
    
            }
        }
        Notification::send( $products ,new ProductAdded($products));
    });
}
        Session::flash('message', 'Product Added Successfully'); 
                         
        return redirect()->back();
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
        $p = Product::where('id' ,$product->id)->first();
        return view('show')->with('p',$p);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
        $products = Product::find($product);
        return view('edit')->with('products',$products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
        DB::transaction(function () use  ($request , $product) {
        $products = Product::find($product->id);
        $products->name = $request->input('name');
        $products->description = $request->input('description');
        $products->price = $request->input('price'); 
        if($request->hasFile('image')){
            $File= $request->file('image');
            $file_name = rand(1, 999) . $File -> getClientOriginalName() ;
            $file = $File->storeAs('Products',$file_name);
        $products->image=$file;
        }
       // dd($products);
        $products->save();
        if($request->hasFile('images')){
            $Files= $request->file('images');
            foreach($Files as $f){
                $file_name = rand(1, 999) . $f -> getClientOriginalName() ;
                $files = $f->storeAs('products',$file_name);
                $gallery= new Gallery;

                $gallery->image= $files;
                $gallery->product_id= $products->id;
                $gallery->save();
    
            }
        }
    });
        
        Session::flash('message', 'Product Updated Successfully'); 
                         
        return redirect()->back();

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $product->delete();
        return redirect()->back();
    }
}
