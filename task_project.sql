-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 21, 2020 at 08:48 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `image` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `description`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Product 100', 'Products/832IMG_9945.jpg', 'description 1', 500, '2020-10-19 21:08:21', '2020-10-21 02:51:02'),
(2, 'Product 2', 'Products/727IMG_9962.jpg', 'description2', 1500, '2020-10-19 21:11:12', '2020-10-19 21:11:12'),
(5, 'car', 'Products/517IMG_9945.jpg', 'car des', 500000, '2020-10-19 21:32:31', '2020-10-19 21:32:31'),
(7, 'product3', 'Products/360IMG_9956-2.jpg', 'asdasdas', 50000, '2020-10-19 23:38:38', '2020-10-20 00:07:37'),
(8, 'asdasdasdasd', 'Products/995IMG_9956-2.jpg', 'asdasdasdas', 55555, '2020-10-20 01:42:12', '2020-10-20 01:42:12'),
(9, 'asdasdas', 'Products/703IMG_9956-2.jpg', 'asdasdasd', 20000, '2020-10-20 01:43:07', '2020-10-20 01:43:07'),
(10, 'asdasdasd', 'Products/17IMG_9960.jpg', 'asdasdasd', 52200, '2020-10-20 01:44:25', '2020-10-20 01:44:25'),
(12, 'asdasd', 'Products/993IMG_9956-2.jpg', 'ssssss', 300, '2020-10-20 01:48:21', '2020-10-20 01:48:21'),
(14, 'asdasd', 'Products/316IMG_9956-2.jpg', 'asdasd', 500, '2020-10-20 02:03:00', '2020-10-20 02:03:00'),
(17, 'asdasd', 'Products/521IMG_9960.jpg', 'asdasd', 500, '2020-10-20 02:16:29', '2020-10-20 02:16:29'),
(18, 'asdasd', 'Products/749IMG_9956-2.jpg', 'asdasd', 300, '2020-10-20 02:18:20', '2020-10-20 02:18:20'),
(19, 'test', 'Products/415IMG_9956-2.jpg', 'asdasd', 500, '2020-10-20 02:22:04', '2020-10-20 02:22:04'),
(21, 'billy', 'Products/778IMG_9945.jpg', 'list and create', 30000, '2020-10-21 01:58:36', '2020-10-21 01:58:36'),
(22, 'apartment', 'Products/307IMG_9992.jpg', 'create and list', 2000, '2020-10-21 02:00:01', '2020-10-21 02:00:01'),
(23, 'billy', 'Products/808IMG_9992.jpg', 'ss', 320, '2020-10-21 13:15:53', '2020-10-21 13:15:53'),
(24, 'asdasd', 'Products/184IMG_9945.jpg', 'asdasddas', 350, '2020-10-21 13:17:07', '2020-10-21 13:17:07'),
(25, 'asdasd', 'Products/911IMG_9956-2.jpg', 'sss', 320, '2020-10-21 13:40:59', '2020-10-21 13:40:59'),
(26, 'hiiiiii', 'Products/799IMG_9956.jpg', 'hiiii', 20000000, '2020-10-21 13:41:46', '2020-10-21 13:41:46'),
(27, 'test billy', 'Products/307IMG_9956.jpg', 'car des', 50000300, '2020-10-21 15:27:13', '2020-10-21 15:27:13'),
(28, 'aaadddassssssss', 'Products/45IMG_9956-2.jpg', 'sasssdasdasdasd', 50005555, '2020-10-21 15:29:00', '2020-10-21 15:29:00'),
(29, 'asdasd', 'Products/497IMG_9956.jpg', 'sssssssssssss', 202020, '2020-10-21 15:35:42', '2020-10-21 15:35:42'),
(30, 'asdasd', 'Products/576IMG_9962.jpg', 'asdasd', 3200, '2020-10-21 16:15:36', '2020-10-21 16:15:36'),
(31, 'asdasdasd', 'Products/482IMG_9992.jpg', 'asdasdasd', 5200, '2020-10-21 16:15:57', '2020-10-21 16:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `image` varchar(191) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `image`, `product_id`) VALUES
(1, 'products/615IMG_9945.jpg', 7),
(2, 'products/764IMG_9956.jpg', 7),
(3, 'products/459IMG_9962.jpg', 7),
(4, 'products/986IMG_9992.jpg', 7),
(5, 'products/127IMG_9956.jpg', 8),
(6, 'products/478IMG_9956-2.jpg', 8),
(7, 'products/611IMG_9960.jpg', 8),
(8, 'products/673IMG_9956-2.jpg', 9),
(9, 'products/165IMG_9960.jpg', 9),
(10, 'products/64IMG_9962.jpg', 9),
(11, 'products/41IMG_9956-2.jpg', 10),
(12, 'products/740IMG_9960.jpg', 10),
(13, 'products/767IMG_9962.jpg', 10),
(16, 'products/680IMG_9960.jpg', 12),
(17, 'products/915IMG_9962.jpg', 12),
(20, 'products/547IMG_9960.jpg', 14),
(21, 'products/610IMG_9962.jpg', 14),
(26, 'products/933IMG_9962.jpg', 17),
(27, 'products/421IMG_9960.jpg', 18),
(28, 'products/354IMG_9960.jpg', 19),
(29, 'products/657IMG_9960.jpg', 20),
(30, 'products/423IMG_9956.jpg', 21),
(31, 'products/605IMG_9956-2.jpg', 22),
(32, 'products/491IMG_9956-2.jpg', 23),
(33, 'products/857IMG_9956-2.jpg', 24),
(34, 'products/522IMG_9992.jpg', 25),
(35, 'products/100IMG_9956-2.jpg', 26),
(36, 'products/280IMG_9960.jpg', 27),
(37, 'products/197IMG_9962.jpg', 27),
(38, 'products/474IMG_9960.jpg', 28),
(39, 'products/338IMG_9962.jpg', 28),
(40, 'products/415IMG_9956.jpg', 29),
(41, 'products/137IMG_9956-2.jpg', 29);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
